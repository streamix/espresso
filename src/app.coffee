express = require 'express'
http = require 'http'
fs = require 'fs'

app = express()

app.configure ->
  app.set 'port', process.env.PORT or 3000
  app.set 'views', "#{__dirname}/../views"
  app.set 'view engine', 'ejs'
  app.use express.favicon()
  app.use express.logger('dev')
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use express.cookieParser('Some secret this is, change this')
  app.use express.session()
  app.use app.router
  app.use(require('less-middleware')({src: "#{__dirname}/../public"}))
  app.use express.static("#{__dirname}/../public")

app.configure 'development', ->
  app.use express.errorHandler()

fs.readdirSync("#{__dirname}/routes").forEach (file) ->
  require("#{__dirname}/routes/#{file}")(app)

http.createServer(app).listen app.get('port'), ->
  console.log "Express server listening on port #{app.get 'port'}"
